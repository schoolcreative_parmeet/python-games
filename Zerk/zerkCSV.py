import sys
import csv

class Place:
    
    #name = string, description = string, exits = list of paths. 
    def __init__(self,name,description,exits,npcs):
        self.name = name
        self.description = description
        self.exits = exits
        self.npcs = npcs

    def render(self):
        print(self.name)
        print(self.description)
        for npc in self.npcs:
            npc.render()
        for path in self.exits:
            path.render()
       

class Path:

    # all parameters are strings.  
    def __init__(self,descriptionString,keyCommandString,destinationName):
        self.description = descriptionString
        self.keyCommand  = keyCommandString
        self.destination = destinationName
        
        
    def render(self):
        print ("[ " + self.keyCommand + " ] " + self.description)



class NPC:

    def __init__(self,name,description,location,greeting):
        self.name = name
        self.description = description
        self.location = location
        self.greeting = greeting
        self.currentLocation = "test"


    def render(self):
        print(self.name)
        print(self.description)
        print(self.name + " says " + self.greeting)


class Player:

    def __init__(self,currentPlaceName):
        self.currentPlaceName = "Hogwarts"

class Game:

    def __init__(self):
        # stores all the places in the game
        self.places = {}
        self.paths =  {}
        self.npcs = {}
        self.player = Player("Hogwarts")
            
    def load_places(self):   
        filename = "Places.csv"
        file = open(filename)
        reader = csv.reader(file)   # object containing the rows

        for row in reader:
            if len(row) < 2:
                print("Bad Row" + str(row))         # each row is a list
            name = row[0]
            description = row[1]
            place_exits = []
            npcs = []
            rowLength = len(row)
            place = Place(name, description, place_exits,npcs)
            self.places[name] = place
            numberOfExits = rowLength -2
            exitIndex = 2

            while exitIndex < rowLength:
                exitName = row[exitIndex]
                exitPath = self.paths[exitName]
                place_exits.append(exitPath)
                exitIndex += 1

    def load_paths(self):
        filename = "Exits.csv"
        file = open(filename)
        reader = csv.reader(file)        
   
        for row in reader:
            if len(row) != 4:
                print ("Invalid Row Detected " + str(row)) 
            # row = name, description
            name = row[0]
            description = row[1]
            keyCommandString = row[2]
            destination = row[3]
            rowLength = len(row)
            path = Path(description, keyCommandString, destination)
            self.paths[name] = path


    def load_npcs(self):
            filename = "NPC.csv"
            file = open(filename)
            reader = csv.reader(file)        
       
            for row in reader:
                # row = name, description
                name = row[0]
                description = row[1]
                location = row[2]
                greeting = row[3]
                npcs = []
                rowLength=len(row)
                npc = NPC(name, description, location, greeting) # CREATES AN NPC OUTPUT
                self.npcs[name] = npc 
                

                    

    def run(self):

        while(1):
            P = self.places[self.player.currentPlaceName]
            P.render()

            for person_name in self.npcs:
                person = self.npcs[person_name] 
                if (person.location == P.name):
                    person.render()

            # self.player.render()
            # -> player's render function print("You have " + str(self.hp) + " hit points")
            # players __init__ add self.hp = 666
            
            player_action = input("-->")

            for E in P.exits:
                if ( E.keyCommand == player_action):                  
                    self.player.currentPlaceName = E.destination
                    
# game loop

theGame = Game()
theGame.load_paths()
theGame.load_places()
theGame.load_npcs()
theGame.run()


